<?php

function google_drive_service_accounts_add() {
  // Create a basic entity structure to be used and passed to the validation
  // and submission functions.
  $entity = entity_get_controller('google_drive_service_account')->create();
  return drupal_get_form('google_drive_service_account_form', $entity);
}

function google_drive_service_account_form($form, &$form_state, $entity) {
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#required' => TRUE,
    '#default_value' => $entity->title,
    '#weight' => -1,
  );

  $form['account_description'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#required' => FALSE,
    '#default_value' => $entity->account_description,
  );

  $form['additional_settings'] = array(
    '#type' => 'vertical_tabs',
  );
  
  $form['google_drive_account_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Google Drive Acount Settings'),
    '#description' => t('Fill with information of your service account created on <a target="_new" href="https://console.developers.google.com">Google Developers Console</a>.'),
    '#group' => 'additional_settings',
  );

  $form['google_drive_account_settings']['client_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Client ID'),
    '#required' => TRUE,
    '#default_value' => $entity->client_id,
    '#group' => 'additional_settings',
  );

  $form['google_drive_account_settings']['email'] = array(
    '#type' => 'textfield',
    '#title' => t('Email address'),
    '#required' => TRUE,
    '#default_value' => $entity->email,
    '#group' => 'additional_settings',
  );

  $form['google_drive_account_settings']['secret_word'] = array(
    '#type' => 'textfield',
    '#title' => t('Secret word'),
    '#description' => t('Usually "notasecret".'),
    '#required' => TRUE,
    '#default_value' => $entity->secret_word,
    '#group' => 'additional_settings',
  );

  $form['google_drive_account_settings']['public_key_fingerprints'] = array(
    '#type' => 'textfield',
    '#title' => t('Public key fingerprints'),
    '#required' => FALSE,
    '#default_value' => $entity->public_key_fingerprints,
    '#group' => 'additional_settings',
  );

  $form['google_drive_account_settings']['key_file'] = array(
    '#type' => 'file',
    '#title' => t('Private key file'),
    '#required' => FALSE,
    '#group' => 'additional_settings',
  );

  $form['google_drive_account_settings']['key_file_desc'] = array(
    '#markup' => t('Current private key file') . ': ' . $entity->key_file_uri,
    '#group' => 'additional_settings',
  );

  $form['google_drive_account_settings']['key_file_uri'] = array(
    '#type' => 'hidden',
    '#required' => FALSE,
    '#default_value' => $entity->key_file_uri,
    '#group' => 'additional_settings',
  );

  $form['google_drive_service_account'] = array(
    '#type' => 'value',
    '#value' => $entity,
  );

  field_attach_form('google_drive_service_account', $entity, $form, $form_state);

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 100,
  );
  $form['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
    '#submit' => array('entity_example_basic_edit_delete'),
    '#weight' => 200,
  );
  
  return $form;
}

/**
 * Validation handler for entity_example_basic_add_form form.
 *
 * We pass things straight through to the Field API to handle validation
 * of the attached fields.
 */
function google_drive_service_account_form_validate($form, &$form_state) {
  $file = file_save_upload('key_file', array(
    // Validate extensions.
    'file_validate_extensions' => array('p12'),
  ));

  // If the file passed validation:
  if ($file) {
    // Move the file into the Drupal file system.
    if ($file = file_move($file, 'private://')) {
      // Save the file for use in the submit handler.
      $form_state['values']['key_file'] = $file;
    }
    else {
      form_set_error('key_file', t("Failed to write the uploaded file to the site's file folder."));
    }
  }
  else {
    // If no file uploaded and key file var empty.
    if ($form_state['values']['key_file_uri'] == '') {
      form_set_error('key_file', t('No file was uploaded.'));
    }
  }
  
  field_attach_form_validate('google_drive_service_account', $form_state['values']['google_drive_service_account'], $form, $form_state);
}

/**
 * Form submit handler: Submits basic_add_form information.
 */
function google_drive_service_account_form_submit($form, &$form_state) {
  $entity = $form_state['values']['google_drive_service_account'];
  
  $file = $form_state['values']['key_file'];
  if ($file) {
    $form_state['values']['key_file_uri'] = $file->uri;
    $file->status = FILE_STATUS_PERMANENT;
    // Save file status.
    file_save($file);    
  }

  $entity->title = $form_state['values']['title'];
  $entity->account_description = $form_state['values']['account_description'];
  $entity->client_id = $form_state['values']['client_id'];
  $entity->email = $form_state['values']['email'];
  $entity->secret_word = $form_state['values']['secret_word'];
  $entity->public_key_fingerprints = $form_state['values']['public_key_fingerprints'];
  $entity->key_file_uri = $form_state['values']['key_file_uri'];

  $entity = google_drive_service_account_save($entity);
  $form_state['redirect'] = 'admin/structure/google-drive-service-accounts/manage';
}

/**
 * Account files import form.
 */
function google_drive_service_import_form($form, &$form_state, $entity) {
  $form = array();

  $form['message'] = array(
    '#markup' => '<p>' . t('If the account has many files, the process can take a long time and can even throw an timeout error.') . '</p><p>' . t('For possible fixes when timeout occurs, check here: <a href="https://api.drupal.org/api/drupal/includes%21common.inc/function/drupal_set_time_limit/7">https://api.drupal.org/api/drupal/includes%21common.inc/function/drupal_set_time_limit/7</a>') . '</p>',
  );  
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Import files'),
  );

  $form['account'] = array(
    '#type' => 'value',
    '#value' => $entity,
  );

  return $form;
}

/**
 * Import Google Drive file form validate.
 */
function google_drive_service_import_form_validate($form, &$form_state) {
}

/**
 * Import Google Drive file form submit.
 */
function google_drive_service_import_form_submit($form, &$form_state) {
  $account = $form_state['values']['account'];
  
  $service = google_drive_service_get_api_service($account);
  
  if (!$service) {
    drupal_set_message(t('Unable to create Google API service. Check your settings.'), 'error');
    return FALSE;
  }
  
  google_drive_service_import_all_files($service, $account);

  drupal_set_message(t('Finished importing files.'));
}

function google_drive_service_import_batch_process($files, $aid) {
  foreach($files as $file) {
    $values = google_drive_service_map_values($file, $aid);
    $parents = $file->getParents();

    db_merge('google_drive_service_file')
      ->key(array('fid' => $values['fid']))
      ->fields($values)
      ->execute();

    foreach ($parents as $parent) {
      db_merge('google_drive_service_file_tree')
        ->key(array('fid' => $values['fid'], 'parent_fid' => $parent->id))
        ->execute();
    }
  }
}

/**
 * Returns a render array with all account entities.
 *
 * In this basic example we know that there won't be many entities,
 * so we'll just load them all for display. See pager_example.module
 * to implement a pager. Most implementations would probably do this
 * with the contrib Entity API module, or a view using views module,
 * but we avoid using non-core features in the Examples project.
 *
 * @see pager_example.module
 */
function google_drive_service_list_accounts() {
  $content = array();
  // Load all of our entities.
  $entities = google_drive_service_account_load_multiple();
  if (!empty($entities)) {
    foreach ($entities as $entity) {
      // Create tabular rows for our entities.
      $rows[] = array(
        'data' => array(
          'id' => $entity->aid,
          'title' => l($entity->title, 'admin/structure/google-drive-service-accounts/manage/' . $entity->aid . ''),
          'description' => $entity->account_description,
          'client_id' => $entity->client_id,
          'edit' => l(t('Edit'), 'admin/structure/google-drive-service-accounts/manage/' . $entity->aid . '/edit'),
          'import' => l(t('Import files'), 'admin/structure/google-drive-service-accounts/manage/' . $entity->aid . '/import'),
        ),
      );
    }
    // Put our entities into a themed table. See theme_table() for details.
    $content['entity_table'] = array(
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => array(t('ID'), t('Title'), t('Description'), t('Client ID'), '', ''),
    );
  }
  else {
    // There were no entities. Tell the user.
    $content[] = array(
      '#type' => 'item',
      '#markup' => t('No accounts currently exist.'),
    );
  }
  return $content;
}
