<?php

/**
 * @file
 */
?>
<div id="google-drive-service-<?php print $variables['id']; ?>">
  <div class="google-drive-service-breadcrumbs">
    <?php print $breadcrumbs; ?>
  </div>
  <?php if(count($files) > 0): ?>
    <ul class="files">
      <?php foreach($files as $file): ?>
      <li>
        <?php if ($file->mime_type == GD_FOLDER_TYPE): ?>
          <?php print l($file->title, 'google-drive-service-ajax-callback/nojs/' . $account_id . '/' . $file->fid . '/' . $root_id . '/' . $show_full_path, array('attributes' => array('class' => 'use-ajax'))); ?>
        <?php else: ?>
          <?php print l($file->title, 'google-drive-service-open/' . $account_id . '/' . $file->fid . '/' . $show_full_path, array('attributes' => array('target' => '_new'))); ?>
        <?php endif; ?>
      </li>
      <?php endforeach; ?>
    </ul>
  <?php else: ?>
    <ul>
      <li><?php print t('No files found.'); ?></li>    
    </ul>
  <?php endif; ?>
</div>