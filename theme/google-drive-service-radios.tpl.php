<?php

/**
 * @file
 * Default theme implementation to present the SKU on a product page.
 */
?>
<h1>TEST</h1>
<div id="<?php print $variables['element']['#id'];?>" class="form-radios">
  <?php foreach($variables['element']['#options'] as $fid => $option): ?>
  <?php
    $checked = $variables['element']['#default_value'] == $fid;
  ?>
  <div class="form-item form-type-radio form-item-<?php print $variables['element']['#name'];?> pad-<?php print $option['level'];?>">
    <input <?php print ($checked?'checked':''); ?> class="form-radio" type="radio" id="<?php print $variables['element']['#id'];?>-<?php print $fid;?>" name="<?php print $variables['element']['#name'];?>" value="<?php print $fid;?>" />
    <label class="option" for="<?php print $variables['element']['#id'];?>-<?php print $fid;?>"><?php print $option['title'];?></label>
  </div>
  <?php endforeach; ?>
</div>