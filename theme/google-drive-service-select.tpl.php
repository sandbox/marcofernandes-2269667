<?php

/**
 * @file
 */
?>
<?php foreach($variables['element']['#options'] as $fid => $title): ?>
  <?php
    $checked = $variables['element']['#default_value'] == $fid;
  ?> 
  <div class="form-item form-type-radio form-item-google-drive-service-files-block-fid">
    <input <?php print ($checked?'checked':''); ?> class="form-radio" type="radio" id="<?php print $variables['element']['#id'];?>-<?php print $fid;?>" name="<?php print $variables['element']['#name'];?>" value="<?php print $fid;?>" />
    <label class="option" for="<?php print $variables['element']['#id'];?>-<?php print $fid;?>"><?php print $title; ?></label>
    <!--
    <?php print l($title, 'google-drive-service-ajax-callback/nojs/' . $variables['element']['#account_id'] . '/' . $fid, array('attributes' => array('class' => 'use-ajax'))); ?>
    -->
  </div>
<?php endforeach; ?>