<?php
$plugin = array(
  'single' => TRUE,  // Just do this one, it is needed.
  'title' => t('Google Drive Folder Content'),  // Title to show up on the pane screen.
  'description' => t('ER MAH GERD, ERT DERS THINS'), // Description to show up on the pane screen.
  'category' => t('Google Drive Service'), // A category to put this under.
  'edit form' => 'google_drive_service_pane_custom_pane_edit_form', // A function that will return the settings form for the pane.
  'render callback' => 'google_drive_service_pane_custom_pane_render', // A function that will return the renderable content.
  'admin info' => 'google_drive_service_pane_custom_pane_admin_info', // A function that will return the information displayed on the admin screen (optional).
  'defaults' => array( // Array of defaults for the settings form.
    'account_id' => '',
    'fid' => '',
    'root_id' => '',
    'show_full_path' => '',
  ),
  'all contexts' => TRUE, // This is NEEDED to be able to use substitution strings in your pane.
);

/**
 * An edit form for the pane's settings.
 */
function google_drive_service_pane_custom_pane_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $account_id = !empty($form_state['values']['account_id']) ? $form_state['values']['account_id'] : $conf['account_id'];
  $account_options = array();
  if (!is_numeric($account_id)) {
    $account_options['0'] = t('Select an account');
  }

  // Load accounts.
  $accounts = google_drive_service_account_load_multiple();
  foreach($accounts as $account) {
    $account_options[$account->aid] = $account->title;
  }

  $form['account_id'] = array(
    '#type' => 'select',
    '#title' => t('Account'),
    '#options' => $account_options,
    '#required' => TRUE,
    '#default_value' => $account_id,
    '#attributes' => array('class' => array('ctools-use-ajax')),
    '#ajax' => array(
      'callback' => 'google_drive_service_pane_custom_pane_edit_form_callback',
      'wrapper' => 'google_drive_service_pane_custom_pane_edit_form',
      'method' => 'replace',
      //'effect' => 'fade',
      'path' => 'system/google-drive-service-pane',
    ),
  );

  $folder_options = array();

  if (isset($account_id) && is_numeric($account_id)) {
    $folders = google_drive_service_get_files($account_id, GD_FOLDER_TYPE, FALSE, NULL, 0);
    $folder_options = array(0 => array('value' => t('Root'), 'class' => array('pad-0')));

    $fid = !empty($form_state['values']['fid']) ? $form_state['values']['fid'] : $conf['fid'];

    $tree = array();
    google_drive_service_get_folder_tree($account_id, $fid, 0, $tree);

    foreach ($folders as $folder) {
      $folder_options[$folder->fid] = array(
        'value' => $folder->title,
        'class' => array('pad-1'),
      );

      google_drive_service_build_folder_options($account_id, $folder->fid, $tree, 0, $folder_options);
    }
  }

  $form['fid'] = array(
    '#type' => 'radios',
    '#title' => t('Folder'),
    //'#required' => TRUE,
    '#prefix' => '<div id="google-drive-service-files-block-fid">',
    '#suffix' => '</div>',
    '#options' => $folder_options,
    '#default_value' => $conf['fid'],
    '#account_id' => $account_id,
    '#attributes' => array('class' => 'ctools-use-ajax'),
    '#ajax' => array(
      'callback' => 'google_drive_service_pane_custom_pane_edit_form_callback',
      'wrapper' => 'google_drive_service_pane_custom_pane_edit_form',
      'method' => 'replace',
      //'effect' => 'fade',
      'path' => 'system/google-drive-service-pane',
    ),
    '#process' => array('google_drive_service_process_radios'),
  );

  $form['#attached'] = array(
    'css' => array(
      'type' => 'file',
      'data' => drupal_get_path('module', 'google_drive_service') . '/theme/google-drive-service-radios.css',
    ),
  );

  return $form;
}

/**
 * Submit function, note anything in the formstate[conf] automatically gets saved
 * Notice, the magic that automatically does that for you.
 */
function google_drive_service_pane_custom_pane_edit_form_submit(&$form, &$form_state) {
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    if (isset($form_state['values'][$key])) {
      $form_state['conf'][$key] = $form_state['values'][$key];
      watchdog('google_drive_service_pane', 'Keys: %keys', array('%keys' => join(',', array_keys($form_state['plugin']['defaults'])), WATCHDOG_NOTICE));
    }
  }
}

/**
 * Run-time rendering of the body of the block (content type)
 * See ctools_plugin_examples for more advanced info
 */
function google_drive_service_pane_custom_pane_render($subtype, $conf, $args, $contexts) {
  $block = new stdClass();

  drupal_add_library('system', 'drupal.ajax');
  $account_id = $conf['account_id'];
  $fid = $conf['fid'];
  $show_full_path = $conf['show_full_path'];

  $folders = google_drive_service_get_files($account_id, GD_FOLDER_TYPE, FALSE, NULL, $fid);
  $files = google_drive_service_get_files($account_id, GD_FOLDER_TYPE, TRUE, NULL, $fid);
  $files = array_merge($folders, $files);

  $output = theme('google_drive_service_files_list', array(
      'account_id' => $account_id,
      'files' => $files,
      'root_id' => $fid,
      'show_full_path' => $show_full_path,
      'breadcrumbs' => google_drive_service_breadcrumbs($account_id, $fid, $fid, $show_full_path))
    );

  // initial content is blank
  $block->title = t('Google Drive Folder Contents'); // This will be overridden by the user within the panel options.
  $block->content = $output;
 
  return $block;
}

/**
 * 'admin info' callback for panel pane.
 */
function google_drive_service_pane_custom_pane_admin_info($subtype, $conf, $contexts) {
  if (!empty($conf)) {
    $block = new stdClass;
    $block->title = $conf['override_title'] ? $conf['override_title_text'] : '';
    $block->content = '';
    return $block;
  }
}